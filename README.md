You first need NPM and NodeJS, then navigate to the project directory.

To install the project dependencies run:

# npm install

Once that is done you can run the project with:

# npm start

This should start the server and launch a browser to http://localhost:3000

You can run built in testing with:

# npm test

Lastly, you can build the project into HTML, JS and CSS files to publish on the web with:

# npm run build
