import React, { Component } from 'react'
import { connect } from 'react-redux'
import { css, style, nthChild } from 'glamor'
import styles from './styles'

class Header extends Component {
  constructor (props) {
    super(props)
    this.state = {
      direction: true
    }
  }

  render () {
    // Non-Responsive Styles
    let fontFamily = style({ fontFamily: 'Open Sans,Helvetica,Arial' })

    let gridStyleAuto = style({
      display: 'grid',
      gridTemplateColumns: 'auto auto auto',
      gridTemplateRows: 'auto'
    })

    let widthStyle = style({ width: '1024px' })
    let gridStyle = css({
      display: 'grid',
      gridTemplateRows: 'auto',
      gridTemplateColumns: '512px 512px'
    })

    return (
      <div
        {...fontFamily}
        style={{ height: '105px', borderBottom: '1px solid #000000' }}
      >
        <div {...gridStyle} {...widthStyle} style={{ margin: '0px auto' }}>
          <div
            {...styles.common.linkHover}
            onClick={() => {
              this.props.functions.changePage('homePage')
            }}
          >
            <h1 style={{ marginTop: '30px', width: '350px' }}>
              Employee Directory
            </h1>
          </div>

          {/* <div>
            <div
              {...gridStyleAuto}
              style={{ margin: '0px auto', marginTop: '40px' }}
            >
              <div
                {...styles.common.linkHover}
                onClick={() => {
                  this.props.functions.changePage('addDepartment')
                }}
              >
                Departments
              </div>

              <div
                {...styles.common.linkHover}
                onClick={() => {
                  this.props.functions.changePage('addEmployee')
                }}
              >
                Employees
              </div>
            </div>
          </div> */}


        </div>
      </div>
    )
  }
}

export default Header
