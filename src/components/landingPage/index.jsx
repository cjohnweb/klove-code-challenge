import React, { Component } from 'react'
import { connect } from 'react-redux'
import { css, style, nthChild } from 'glamor'
import theStyles from './styles'

import TopBar from './topBar'
import Header from './header'
import Slider from './slider/slider'
import Footer from './footer'
import { slide as Menu } from 'react-burger-menu'


class LandingPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      page: 'homePage'
    }

    this.changePage = this.changePage.bind(this)
  }

  changePage (page) {
    this.setState({ page })
  }

  render () {
    // Styles
    let fontFamily = style({ fontFamily: 'Open Sans,Helvetica,Arial' })

    // Pass down functions to child components
    let functions = {
      changePage: this.changePage
    }

    return (
      <div {...fontFamily} style={{ width: '100%' }}>
        <Header functions={functions} />
        <Slider
          setAuthentication={this.props.setAuthentication}
          page={this.state.page}
        />
        <Footer functions={functions} />
      </div>
    )
  }
}

export default LandingPage
