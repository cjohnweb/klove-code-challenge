import React from 'react'
import { connect } from 'react-redux'
import { style, nthChild } from 'glamor'

class Department extends React.Component {
  constructor (props) {
    super(props)
    this.state = {}

    this.addNewEmployee = this.addNewEmployee.bind(this)
  }

  addNewEmployee () {
    let employees = this.props.employees
    let index = employees.length

    let newEmployee = {
      id: index,
      profile: '',
      name: 'New Guy',
      position: '',
      company: 'K-LOVE & Air1',
      department: this.props.department,
      phone: '',
      site: '',
      email: ''
    }

    this.props.functions.addEmployee(newEmployee)
  }

  render () {
    // Styles
    let altBg = style(
      nthChild('even', {
        padding: '10px',
        fontSize: '1.5rem',
        backgroundColor: '#ffffff',
        transition: 'background-color 0.25s ease',
        ':hover': {
          backgroundColor: '#e6e6e6'
        }
      }),
      nthChild('odd', {
        padding: '10px',
        fontSize: '1.5rem',
        backgroundColor: '#f8f8f8',
        transition: 'background-color 0.25s ease',
        ':hover': {
          backgroundColor: '#e6e6e6'
        }
      })
    )

    let department = this.props.department

    let employees = this.props.employees
    employees = this.props.employees
      .filter((employee, i) => {
        if (!employee) {
          return false
        }
        return employee.department == department
      })
      .map((employee, i) => {
        return (
          <div
            {...altBg}
            key={i}
            data-id={employee.id}
            onClick={e => {
              this.props.functions.selectEmployee(e)
            }}
          >
            {employee.name}
          </div>
        )
      })

    return (
      <div>
        <div
          style={{
            display: 'grid',
            gridTemplateRows: 'auto',
            gridTemplateColumns: '33% 33% 33%'
          }}
        >
          <div
            style={{ textAlign: 'left', marginLeft: '20px' }}
            onClick={() => {
              this.props.functions.selectDepartment(null)
            }}
          >
            <i
              className='fa fa-2x fa-angle-double-left'
              style={{ color: '#ffd289', fontSize: '4rem' }}
            />
          </div>

          <div>
            <h2>{department} Directory</h2>
          </div>

          {this.props.editDepartment ? (
            <span style={{ textAlign: 'right', marginRight: '24px' }}>
              <i
                className='fa fa-2x fa-user-plus'
                style={{ color: '#efefef', fontSize: '4rem' }}
              />
            </span>
          ) : (
            <span
              style={{ textAlign: 'right', marginRight: '24px' }}
              onClick={() => {
                this.addNewEmployee()
              }}
            >
              <i
                className='fa fa-2x fa-user-plus'
                style={{ color: '#ffd289', fontSize: '4rem' }}
              />
            </span>
          )}
        </div>

        <div>{employees}</div>
      </div>
    )
  }
}

export default Department
