import React from 'react'
import { connect } from 'react-redux'
import { style, nthChild } from 'glamor'

class Departments extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      addDepartment: false,
      newDepartment: '',
      editDepartmentId: null,
      edit: ''
    }

    this.finalizeUpdate = this.finalizeUpdate.bind(this)
    this.cancelUpdate = this.cancelUpdate.bind(this)
    this.editDepartment = this.editDepartment.bind(this)
    this.updateDepartment = this.updateDepartment.bind(this)
    this.updateNewDepartment = this.updateNewDepartment.bind(this)
    this.saveDepartmentEdit = this.saveDepartmentEdit.bind(this)
  }

  finalizeUpdate (e) {
    this.props.functions.updateDepartment(
      this.state.editDepartmentId,
      this.state.edit
    )
    this.setState({ editDepartmentId: null, edit: null })
  }

  cancelUpdate () {
    this.setState({ edit: '', editDepartmentId: null })
  }

  editDepartment (id, value) {
    this.setState({ edit: value, editDepartmentId: id })
  }

  updateDepartment (e) {
    let value = e.target.value
    this.setState({ edit: value })
  }

  updateNewDepartment (e) {
    let value = e.target.value
    this.setState({ newDepartment: value })
  }

  saveDepartmentEdit () {}

  render () {
    // Styles
    let altBg = style(
      nthChild('even', {
        padding: '10px',
        fontSize: '1.5rem',
        backgroundColor: '#ffffff',
        transition: 'background-color 0.25s ease',
        ':hover': {
          backgroundColor: '#e6e6e6'
        }
      }),
      nthChild('odd', {
        padding: '10px',
        fontSize: '1.5rem',
        backgroundColor: '#f8f8f8',
        transition: 'background-color 0.25s ease',
        ':hover': {
          backgroundColor: '#e6e6e6'
        }
      })
    )

    // Grab all departments
    // console.log('Departments ', this.props.departments)

    let departments = this.props.departments

    departments = departments.map((department, i) => {
      if (!department) {
        return null
      }
      if (i == this.state.editDepartmentId) {
        return (
          <div {...altBg} key={i}>
            <input
              style={{ width: '400px', height: '40px', fontSize: '1.5rem' }}
              data-id={i}
              onChange={e => {
                this.updateDepartment(e)
              }}
              value={this.state.edit}
            />

            <button
              data-id={this.state.editDepartmentId}
              data-department={this.state.edit}
              style={{ width: '100px', height: '40px', fontSize: '1.5rem' }}
              onClick={e => {
                this.finalizeUpdate(e)
              }}
            >
              Save
            </button>

            <button
              style={{ width: '100px', height: '40px', fontSize: '1.5rem' }}
              onClick={() => {
                this.cancelUpdate()
              }}
            >
              Cancel
            </button>

            <button
              style={{ width: '100px', height: '40px', fontSize: '1.5rem' }}
              onClick={() => {
                this.props.functions.deleteDepartment(
                  this.state.editDepartmentId
                )
              }}
            >
              Delete
            </button>
          </div>
        )
      } else {
        return (
          <div
            {...altBg}
            key={i}
            style={{
              display: 'grid',
              gridTemplateColumns: 'auto 50px',
              gridTemplateRows: 'auto'
            }}
          >
            <div
              data-id={i}
              data-department={department}
              onClick={e => {
                this.props.functions.selectDepartment(department)
              }}
            >
              {department}
            </div>

            <div>
              <span
                onClick={() => {
                  this.editDepartment(i, department)
                }}
              >
                <i className={'fa fa-pencil'} />
              </span>
            </div>
          </div>
        )
      }
    })

    return (
      <div>
        <div
          style={{
            display: 'grid',
            gridTemplateRows: 'auto',
            gridTemplateColumns: 'auto 80px'
          }}
        >
          <h2>Departments</h2>

          {this.props.editDepartment ? (
            <span style={{ textAlign: 'right', marginRight: '24px' }}>
              <i
                className='fa fa-2x fa-plus'
                style={{ color: '#efefef', fontSize: '4rem' }}
              />
            </span>
          ) : (
            <span
              style={{ textAlign: 'right', marginRight: '24px' }}
              onClick={() => {
                this.setState({ addDepartment: true })
              }}
            >
              <i
                className='fa fa-2x fa-plus'
                style={{ color: '#ffd289', fontSize: '4rem' }}
              />
            </span>
          )}
        </div>

        <div>{departments}</div>

        {this.state.addDepartment ? (
          <div {...altBg}>
            <input
              type={'text'}
              style={{ width: '400px', height: '32px', fontSize: '1.5rem' }}
              onChange={e => {
                this.updateNewDepartment(e)
              }}
              value={this.state.newDepartment}
            />
            <button
              style={{ width: '100px', height: '32px', fontSize: '1.5rem' }}
              onClick={() => {
                this.props.functions.addDepartment(this.state.newDepartment)
                this.setState({ addDepartment: false, newDepartment: '' })
              }}
            >
              Save
            </button>

            <button
              style={{ width: '100px', height: '32px', fontSize: '1.5rem' }}
              onClick={() => {
                this.setState({ addDepartment: false, newDepartment: '' })
              }}
            >
              Cancel
            </button>
          </div>
        ) : null}
      </div>
    )
  }
}

export default Departments
