// Core
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { style } from 'glamor'

// Helper
import { isURLValid, formatURL } from '../../../shared/validation'

// Style
import styles from '../modal_styles'

class EditEmployee extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      employee: this.props.employees.find((employee, i) => {
        return employee.id == this.props.employeeId
      }),
      validZip: true
    }

    this.uploadFile = this.uploadFile.bind(this)
    this.updateField = this.updateField.bind(this)
    this.phoneField = this.phoneField.bind(this)
    this.updateWebsite = this.updateWebsite.bind(this)
    this.saveEmployeeProfile = this.saveEmployeeProfile.bind(this)

    this.changes = false
  }

  uploadFile (e) {
    let file = e.target.files.length ? e.target.files[0] : null
    if (file.size > 2097152) {
      alert(
        'The file is too large. Please pick a profile that is less than 2 mb.'
      )
    } else if (file) {
      this.props.addImage(file)
    }
  }

  phoneField (e) {
    let value = e.target.value
    if (value === '') {
      let employee = this.state.employee
      employee.phone = value
      this.changes = true
      this.setState({ employee })
    } else {
      if (
        e.target.value.length > 1 &&
        !/[)]/.test(e.target.value) &&
        e.target.value.length < 6
      ) {
        value = value.slice(0, value.length - 1)
      }
      value = value.replace(/[( )-]/g, '')
      let regexp = /[^0-9]/
      if (!regexp.test(value)) {
        if (value.length > 0) {
          value = '(' + value
        }
        if (value.length === 0) {
          // do nothing
        } else if (value.length < 5) {
          value = value + ')'
        } else {
          value = value.slice(0, 4) + ') ' + value.slice(4)
        }
        if (value.length > 9) {
          value = value.slice(0, 9) + '-' + value.slice(9)
        }
        let employee = this.state.employee
        employee.phone = value
        this.changes = true
        this.setState({ employee })
      }
    }
  }

  updateField (e) {
    // Get the changes and which field those changes are from
    let field = e.target.dataset.field
    let value = e.target.value
    // Break out address down into various city, state, zip, etc fields.
    let employee = this.state.employee
    employee[field] = value
    this.changes = true
    this.setState({ employee })
  }

  updateWebsite (e) {
    // Get the changes and which field those changes are from
    let field = e.target.dataset.field
    let value = e.target.value
    let employee = this.state.employee
    employee[field] = value
    this.changes = true
    this.setState({ employee })
  }

  saveEmployeeProfile () {
    if (!this.changes) {
      this.props.functions.editEmployee(null)
    } else {
      let employee = this.state.employee
      // save changes
      this.props.functions.updateEmployee(employee)
      this.props.functions.editEmployee(null)
    }
  }

  render () {
    let employeeId = this.props.employeeId
    let employees = this.props.employees
    let employee = this.state.employee

    let departmentOptions = this.props.departments.map(department => {
      if (!department) {
        return null
      }
      return (
        <option key={department} value={department}>
          {department}
        </option>
      )
    })

    // let profile = formatURL(employee.profile) // employee.profile is either a url or base64 encoded string
    // return { isURL, isB64, url, link, payload }

    // if (profile.isURL) {
    //   profile = (
    //     <img src={profile.url} style={{ maxWidth: '200px', height: '100px' }} />
    //   )
    // } else if (profile.isB64) {
    //   profile = (
    //     <img
    //       src={profile.payload}
    //       style={{ maxWidth: '200px', height: '100px' }}
    //     />
    //   )
    // } else {
    //   profile = (
    //     <div
    //       style={{
    //         width: '200px',
    //         height: '100px',
    //         display: 'flex',
    //         alignItems: 'center',
    //         justifyContent: 'center'
    //       }}
    //     >
    //       <span
    //         className='fa fa-2x fa-building'
    //         style={{ color: '#efefef', fontSize: '4rem' }}
    //       />
    //     </div>
    //   )
    // }

    return (
      <div {...styles.content}>
        <div {...styles.employeeButtonsSexy}> &nbsp; </div>
        <div {...styles.card}>
          <div {...styles.columns}>
            <div {...styles.form}>
              <div {...styles.smallBox350}>
                <label {...styles.labelHeader}>
                  <span {...styles.logoHeader}>
                    <span className='fa fa-2x fa-building' /> &nbsp;{' '}
                    <span>Edit Employee</span>
                  </span>
                </label>

                {/* <label {...styles.label} htmlFor={'uploadLogo'}>
                  <span {...styles.clearTextLabelMiddle}>Click to edit</span>
                  <span {...styles.clearTextLabel}>{profile}</span>
                  <input
                    {...styles.LogoInput}
                    style={{ display: 'none' }}
                    type='file'
                    accept='image/*'
                    id={'uploadLogo'}
                    onChange={this.uploadFile}
                  />
                </label> */}

                {/* <label {...styles.label}>
                  <span
                    {...styles.smallNoticeText}
                    style={{ marginBottom: '20px' }}
                  >
                    Use SVG, PNG, GIF or JPG. Max 350KB, upto 400 x 400 pixels.
                  </span>
                </label> */}

                <label {...styles.label}>
                  <span {...styles.clearTextLabel}>Name</span>
                  <input
                    {...styles.inputSexy}
                    value={employee.name}
                    data-field={'name'}
                    onChange={this.updateField}
                  />
                </label>

                <label {...styles.label}>
                  <span {...styles.clearTextLabel}>Company</span>
                  <input
                    {...styles.inputSexy}
                    value={employee.company}
                    data-field={'company'}
                    onChange={this.updateField}
                  />
                </label>

                <label {...styles.label}>
                  <span {...styles.clearTextLabel}>Department</span>
                  <select
                    {...styles.inputSexy}
                    value={employee.department}
                    data-field={'department'}
                    onChange={this.updateField}
                  >
                    <option value={''} />
                    {departmentOptions}
                  </select>
                </label>

                <label {...styles.label}>
                  <span {...styles.clearTextLabel}>Position</span>
                  <input
                    {...styles.inputSexy}
                    value={employee.position}
                    data-field={'position'}
                    onChange={this.updateField}
                  />
                </label>

                <label {...styles.label}>
                  <span {...styles.clearTextLabel}>Email</span>
                  <input
                    {...styles.inputSexy}
                    value={employee.email}
                    data-field={'email'}
                    onChange={this.updateField}
                  />
                </label>

                <label {...styles.label}>
                  <span {...styles.clearTextLabel}>Phone</span>
                  <input
                    {...styles.inputSexy}
                    type='text'
                    maxLength='14'
                    value={employee.phone || ''}
                    onChange={this.phoneField}
                  />
                </label>

                <label {...styles.label}>
                  <span {...styles.clearTextLabel}>Website</span>
                  <input
                    {...styles.inputSexy}
                    type='url'
                    data-field='site'
                    value={employee.site || ''}
                    onChange={this.updateWebsite}
                  />
                </label>
              </div>
            </div>
          </div>
        </div>
        <div {...styles.buttons}>
          <div {...styles.button} onClick={this.saveEmployeeProfile}>
            Save
          </div>
          <div
            {...styles.button}
            onClick={() => this.props.functions.editEmployee(null)}
          >
            Cancel
          </div>
        </div>
      </div>
    )
  }
}

export default EditEmployee
