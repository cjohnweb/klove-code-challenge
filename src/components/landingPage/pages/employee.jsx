import React from 'react'
import { style, nthChild } from 'glamor'
import { isURLValid, formatURL } from '../../../shared/validation'
import styles from '../modal_styles'

class Employee extends React.Component {
  constructor (props) {
    super(props)
    this.state = {}
    this.deleteEmployee = this.deleteEmployee.bind(this)
  }

  deleteEmployee () {
    let c = window.confirm(
      'Are you sure you want to permanently delete this employee?'
    )
    if (c) {
      this.props.functions.deleteEmployee(this.props.employeeId)
      this.props.functions.resetEmployee()
    }
  }

  render () {
    console.log('Styles: ', styles)
    let id = this.props.employeeId
    let employee = this.props.employees.find(employee => {
      if (!employee) {
        return false
      }
      return employee.id == id
    })

    if (!employee) {
      return (
        <div>
          <p>
            There was an error, employee with ID {id} doesn't seem to exist.
          </p>
        </div>
      )
    }

    console.log('Employee: ', employee)
    // Handle Employee Profile Picture
    let profile
    if (employee.profile) {
      let validProfile = formatURL(employee.profile)
      profile = (
        <img
          src={validProfile.isURL ? validProfile.url : validProfile.payload}
          style={{ maxWidth: '200px', maxHeight: '100px' }}
        />
      )
    } else {
      profile = (
        <div
          style={{
            width: '200px',
            height: '60px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <span
            className='fa fa-2x fa-building'
            style={{ color: '#efefef', fontSize: '4rem' }}
          />
        </div>
      )
    }

    let site = formatURL(employee.site)

    return (
      <div {...styles.content}>
        <div {...styles.card}>
          <div {...styles.columns}>
            <div {...styles.smallBox450}>
              <label {...styles.labelHeader}>
                <span {...styles.profileHeader}>
                  <span className='fa fa-2x fa-building' /> &nbsp;{' '}
                  <span>{employee.name}</span>
                </span>
              </label>

              <label
                {...styles.label}
                onClick={() => {
                  this.props.functions.editEmployee(this.props.employeeId)
                }}
              >
                <span {...styles.clearTextLabelMiddle}>Profile: </span>
                <span {...styles.clearText}>
                  <div {...styles.logoContainer}>{profile}</div>
                </span>
              </label>

              <label {...styles.label}>
                <span {...styles.clearTextLabel}>Company: </span>
                <span {...styles.clearText}>{employee.company}</span>
                <br />
              </label>

              <label {...styles.label}>
                <span {...styles.clearTextLabel}>Department: </span>
                <span {...styles.clearText}>{employee.department}</span>
                <br />
              </label>

              <label {...styles.label}>
                <span {...styles.clearTextLabel}>Position: </span>
                <span {...styles.clearText}>{employee.position}</span>
                <br />
              </label>

              <label {...styles.label}>
                <span {...styles.clearTextLabel}>Email: </span>
                <span {...styles.clearText}>{employee.email}</span>
                <br />
              </label>

              <label {...styles.label}>
                <span {...styles.clearTextLabel}>Phone: </span>
                <span {...styles.clearText}>
                  <a href={'tel:' + employee.phone}>{employee.phone}</a>
                </span>
                <br />
              </label>

              <label {...styles.label}>
                <span {...styles.clearTextLabel}>Website: </span>
                <span {...styles.clearText}>
                  <a href={site.url} target={'_blank'}>
                    {site.link}
                  </a>
                </span>
                <br />
              </label>
            </div>
          </div>
        </div>

        <div {...styles.buttons}>
          <div
            {...styles.button}
            onClick={() => {
              this.props.functions.editEmployee(this.props.employeeId)
            }}
          >
            Edit
          </div>
          <div
            {...styles.button}
            onClick={() => {
              this.deleteEmployee()
            }}
          >
            Delete
          </div>
          <div
            {...styles.button}
            onClick={() => {
              this.props.functions.resetEmployee()
            }}
          >
            Done
          </div>
        </div>
      </div>
    )
  }
}

export default Employee
