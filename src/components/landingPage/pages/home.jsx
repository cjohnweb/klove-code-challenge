import React, { Component } from 'react'
import { connect } from 'react-redux'
import { css, style, nthChild, nthLastChild } from 'glamor'
// Pages
import Departments from './departments'
import Department from './department'
import Employee from './employee'
import EditEmployee from './editEmployee'
import UnassignedEmployees from './unassignedEmployees'

class HomePage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      department: null,
      employee: null,
      editEmployee: null,
      editDepartment: null,
      departments: ['Engineering', 'Software', 'Human Resources'],
      employees: [
        {
          id: 0,
          profile: '',
          name: 'John Minton',
          position: 'Frontend Developer',
          company: 'Educational Media Foundation - K-LOVE & Air1',
          phone: '(530) 454-5646',
          site: 'http://johnminton.info',
          email: 'cjohnweb@gmail.com',
          department: 'Software'
        },
        {
          id: 1,
          profile: '',
          name: 'James Mason',
          position: 'Engineering Applications Supervisor',
          company: 'Educational Media Foundation - K-LOVE & Air1',
          phone: '(916) 251-1835',
          site: '',
          email: 'jmason@kloveair1.com',
          department: 'Engineering'
        }
      ]
    }
    this.addDepartment = this.addDepartment.bind(this)
    this.deleteDepartment = this.deleteDepartment.bind(this)
    this.updateDepartment = this.updateDepartment.bind(this)
    this.editDepartment = this.editDepartment.bind(this)

    this.addEmployee = this.addEmployee.bind(this)
    this.deleteEmployee = this.deleteEmployee.bind(this)
    this.updateEmployee = this.updateEmployee.bind(this)

    this.selectDepartment = this.selectDepartment.bind(this)
    this.selectEmployee = this.selectEmployee.bind(this)
    this.resetEmployee = this.resetEmployee.bind(this)

    this.editEmployee = this.editEmployee.bind(this)
  }

  addDepartment (newDepartment) {
    let departments = this.state.departments
    departments.push(newDepartment)
    this.setState({ departments })
  }

  deleteDepartment (departmentId) {
    let departments = this.state.departments

    // alert('Departments: ' + departments)
    // alert('DepartmentId: ' + departmentId)
    // alert('Departments[departmentId]: ' + departments[departmentId])

    let c = window.confirm(
      'Are you sure you want to delete this department ' +
        departments[departmentId] +
        '? (This will not remove employees)'
    )
    if (c) {
      departments[departmentId] = null
      this.setState({ departments })
    }
  }

  updateDepartment (id, value) {
    let departments = this.state.departments
    departments[id] = value
    this.setState({ departments })
  }

  editDepartment (departmentId) {
    this.setState({ editDepartment: departmentId })
  }

  addEmployee (newEmployee) {
    let employees = this.state.employees
    employees.push(newEmployee)
    this.setState({ employees })
  }

  deleteEmployee (employeeId) {
    let employees = this.state.employees
    employees[employeeId] = null
    this.setState({ employees })
  }

  updateEmployee (newEmployee) {
    let id = newEmployee.id
    let employees = this.state.employees
    employees[id] = newEmployee
    this.setState({ employees })
  }

  selectDepartment (department) {
    this.setState({ department })
  }

  selectEmployee (e) {
    let id = e.target.dataset.id
    // alert("ID: "+id)
    this.setState({ employee: id })
  }

  resetEmployee () {
    this.setState({ employee: null })
  }

  editEmployee (employeeId) {
    this.setState({ editEmployee: employeeId })
  }

  render () {
    // Grab all departments
    let departments = this.state.departments
    let employees = this.state.employees
    employees = employees.map((employee, i) => {
      if (!employee) {
        return null
      }
      employee.id = i // assign original id for reference
      return employee
    })

    let department = this.state.department

    let editDepartment = this.state.editDepartment
    let employee = this.state.employee

    let functions = {
      addDepartment: this.addDepartment,
      deleteDepartment: this.deleteDepartment,
      updateDepartment: this.updateDepartment,
      editDepartment: this.editDepartment,
      addEmployee: this.addEmployee,
      deleteEmployee: this.deleteEmployee,
      updateEmployee: this.updateEmployee,
      selectDepartment: this.selectDepartment,
      selectEmployee: this.selectEmployee,
      resetEmployee: this.resetEmployee,
      editEmployee: this.editEmployee
    }

    return (
      <div style={{ margin: '0px auto', width: '800px' }}>
        {!isNaN(parseInt(this.state.editEmployee)) ? (
          <EditEmployee
            functions={functions}
            department={department}
            departments={departments}
            employeeId={employee}
            employees={employees}
          />
        ) : !isNaN(parseInt(employee)) ? (
          <Employee
            functions={functions}
            department={department}
            employeeId={employee}
            employees={employees}
          />
        ) : department ? (
          <Department
            functions={functions}
            department={department}
            employees={employees}
          />
        ) : (
          <div>
            <Departments
              editDepartment={editDepartment}
              functions={functions}
              departments={departments}
            />
            <UnassignedEmployees
              functions={functions}
              departments={departments}
              employees={employees}
            />
          </div>
        )}
      </div>
    )
  }
}

export default connect(
  (state, ownProps) => ({}),
  dispatch => ({})
)(HomePage)
