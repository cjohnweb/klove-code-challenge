import React from 'react'
import { connect } from 'react-redux'
import { style, nthChild } from 'glamor'

class UnassignedEmployees extends React.Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  render () {
    // Styles
    let altBg = style(
      nthChild('even', {
        padding: '10px',
        fontSize: '1.5rem',
        backgroundColor: '#ffffff',
        transition: 'background-color 0.25s ease',
        ':hover': {
          backgroundColor: '#e6e6e6'
        }
      }),
      nthChild('odd', {
        padding: '10px',
        fontSize: '1.5rem',
        backgroundColor: '#f8f8f8',
        transition: 'background-color 0.25s ease',
        ':hover': {
          backgroundColor: '#e6e6e6'
        }
      })
    )

    let departments = this.props.departments
    let employees = this.props.employees
      .filter((employee, i) => {
        if (!employee) {
          return false
        }
        return !departments.includes(employee.department)
      })
      .map((employee, i) => {
        return (
          <div
            {...altBg}
            key={i}
            data-id={employee.id}
            onClick={e => {
              this.props.functions.selectEmployee(e)
            }}
          >
            {employee.name}
          </div>
        )
      })

    return (
      <div>
        {employees.length > 0 ? (
          <div>
            <h2>Employees witout Department</h2>
            <div>{employees}</div>
          </div>
        ) : null}
      </div>
    )
  }
}

export default UnassignedEmployees
