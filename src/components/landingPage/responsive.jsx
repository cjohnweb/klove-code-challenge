/**
 * This is an example of responsive behavior 
 * 
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { css, style, nthChild } from 'glamor';
import TopBar from './topBar';

class Responsive extends Component {
  constructor(props) {
    super(props);
    this.state = {
      direction: true
    };

    this.windowResize = this.windowResize.bind(this);

  }

  componentWillMount() {
    window.addEventListener("resize", this.windowResize);
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.windowResize);
  }


  windowResize() {
    console.log("Window Resize");
    this.forceUpdate()
  }

  render() {

    // Responsive Styles
    let widthStyle;
    let logoWidthStyle;
    let gridStyle = css({ display: 'grid', gridTemplateRows: 'auto' });


    // Responsive Action
    widthStyle = style({ width: '1024px' });
    gridStyle = css(gridStyle, {
      gridTemplateColumns: '512px 512px'
    });
    logoWidthStyle = css({ width: '350px' })

    // Responsive Break Points
    const breakpoints = {
      desktop: 1024,
      tablet: 840,
      mobile: 540
    };

    if (window.innerWidth >= breakpoints.desktop) {

      widthStyle = style({ width: '1024px' });
      gridStyle = css(gridStyle, {
        gridTemplateColumns: '512px 512px'
      });

    } else if (window.innerWidth >= breakpoints.tablet) {

      widthStyle = style({ width: '840px' });
      gridStyle = css(gridStyle, {
        gridTemplateColumns: '420px 420px',
      });


    } else if (window.innerWidth >= breakpoints.mobile) {

      widthStyle = style({ width: '540px' });
      gridStyle = css(gridStyle, {
        gridTemplateColumns: '270px 270px'
      });

    } else if (window.innerWidth <= breakpoints.mobile) {

      widthStyle = style({ width: '540px' });
      gridStyle = css(gridStyle, {
        gridTemplateColumns: '270px 270px'
      });

    }



    // Non-Responsive Styles
    let fullWidthStyle = style({ width: '100%' })
    let fontFamily = style({ fontFamily: 'Open Sans,Helvetica,Arial' });
    let gridStyleAuto = style({
      display: 'grid',
      gridTemplateColumns: 'auto auto auto',
      gridTemplateRows: 'auto'
    });

    return (<div {...fontFamily} {...fullWidthStyle}>

      <div {...fullWidthStyle} style={{ background: '#39394d', height: '40px' }}>
        <TopBar />
      </div>


      <div {...fullWidthStyle} style={{ height: '105px', borderBottom: '1px solid #000000' }}>

        <div {...gridStyle} {...widthStyle} style={{ margin: '0px auto' }}>

          <div><img src='./logo.png' {...logoWidthStyle} /></div>

          <div {...fontFamily} style={{}}>

            <div {...gridStyleAuto} style={{ margin: '0px auto', marginTop: '40px' }}>
              <div style={{}}>Sign In</div>
              <div style={{}}>Plans &amp; Pricing</div>
              <div style={{}}>Contact Sales</div>
            </div>

          </div>

        </div>

      </div>

      <div {...fullWidthStyle} style={{ backgroundImage: 'url(banner.jpg)', backgroundColor: '#333333', height: '350px' }}>

        <div style={{ color: '#ffffff', width: '500px', margin: '0px auto', paddingTop: '100px' }}>
          <p>WorkorderUp software enables you to</p>
          <ul>
            <li>Expand your business</li>
            <li>Keep track of customer utilities</li>
            <li>Retain customer for repeat business</li>
          </ul>
        </div>

      </div>

      <div {...fullWidthStyle} style={{ backgroundColor: '#efefef', height: '200px' }}>

      </div>


    </div >);

  }
}

export default Responsive;
