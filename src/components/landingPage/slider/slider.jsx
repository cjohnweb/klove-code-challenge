import React from 'react';
import { connect } from 'react-redux';
import { style } from 'glamor';

// Main Navigational "Pages" / Components
import HomePage from '../pages/home';
// import Employees from '../pages/employees';
// import Departments from '../pages/departments';

class Slider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: "homePage",
      next: null,
      opacity: 0,
      opacity2: 0
    };

    this.sort = {
      homePage: 1,
      employees: 2,
      departments: 3,
      };

    this.changePage = this.changePage.bind(this);
    this.changePageByName = this.changePageByName.bind(this);

  }

  componentDidMount() {
    // Page fade in
    window.setTimeout(() => {
      this.setState({ opacity: "1" });
    }, 10);
    window.setTimeout(() => {
      this.setState({ opacity2: "1" });
    }, 500);
  }

  componentDidUpdate() {
    if (this.state.next) {
      window.setTimeout(() => {
        this.refs.slider.style.transition = "left 0.5s ease";
        this.refs.slider.style.left = (this.sort[this.state.page] < this.sort[this.state.next] ? "-100%" : "0");
      }, 20);
      window.setTimeout(() => {
        //this.refs.slider.style.removeProperty('transition');
        this.refs.slider.style.removeProperty('left');
      }, 520);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.page && this.props.page != nextProps.page) {
      this.changePageByName(nextProps.page);
    }
  }

  changePageByName(page) {
    if (!this.state.next && this.state.page !== page) {
      this.setState({ next: page });
      window.setTimeout(() => {
        this.setState({ page: page, next: null });
      }, 520);
    }
  }


  changePage(e) {
    let page = e.currentTarget.dataset.page;
    if (!this.state.next && this.state.page !== page) {
      this.setState({ next: page });
      window.setTimeout(() => {
        this.setState({ page: page, next: null });
      }, 520);
    }
  }

  render() {
    const sliderStyles = {
      slider: {
        container: style({
          padding: "0",
          margin: "0",
          top: "0",
          left: "0",
          width: "100%",
          height: "100%",
          fontFamily: "'Futura Light', Arial",
          color: "#000000",
          backgroundSize: "cover"
          // WebkitBackgroundSize: "cover",
          // MozBackgroundSize: "cover",
          // OBackgroundSize: "cover"
        }),
        main: style({
          width: "100%",
          height: "Calc(100% - 64px)",
          overflow: "hidden"
        }),
        slideContainer: style({
          height: "100%",
          width: "100%",
          overflow: "hidden",
          position: "fixed",
          top: "0",
          left: "0",
          width: "100%",
          height: "100%",
          zIndex: "1",
        }),
        slideStatic: style({
          height: "100%",
          width: "100%",
          margin: "0px",
          padding: "0px",
        }),
        slideRight: style({
          position: "relative",
          display: "grid",
          height: "100%",
          width: "200%",
          gridTemplateColumns: "50% 50%",
          gridTemplateRows: "100%",
          left: "-100%"
        }),
        slideLeft: style({
          position: "relative",
          display: "grid",
          height: "100%",
          width: "200%",
          gridTemplateColumns: "50% 50%",
          gridTemplateRows: "100%",
          left: "0"
        }),
        slideUp: style({
          position: "relative",
          display: "grid",
          height: "200%",
          width: "100%",
          gridTemplateRows: "50% 50%",
          gridTemplateColumns: "100%",
          top: "-100%"
        }),
        slideDown: style({
          position: "relative",
          display: "grid",
          height: "200%",
          width: "100%",
          gridTemplateRows: "50% 50%",
          gridTemplateColumns: "100%",
          top: "0%"
        })
      }
    };

    // List of variables named after page names
    let homePage;
    // let employees;
    // let departments;

    // If we are rendering a page, or sliding to / from a page, render it
    if (this.state.page === "homePage" || this.state.next === "homePage") {
      homePage = (
        <HomePage functions={this.props.functions} changePage={this.changePage} changePageByName={this.changePageByName} />
      );
    }


    // if (this.state.page === "employees" || this.state.next === "employees") {
    //   employees = (
    //     <Employees setAuthentication={this.props.setAuthentication} functions={this.props.functions} changePage={this.changePage} changePageByName={this.changePageByName} />
    //   );
    // }


    // if (this.state.page === "departments" || this.state.next === "departments") {
    //   departments = (
    //     <Departments functions={this.props.functions} changePage={this.changePage} changePageByName={this.changePageByName} />
    //   );
    // }

    // Now get that slide magic in there
    let slideStyle;
    if (this.state.next) {
      slideStyle = (this.sort[this.state.page] < this.sort[this.state.next] ? sliderStyles.slider.slideLeft : sliderStyles.slider.slideRight);
    } else {
      slideStyle = sliderStyles.slider.slideStatic;
    }

    let content = (
      <div ref="slider" {...slideStyle} style={{ minHeight: '400px' }}>
        {homePage}
        {/* {employees} */}
        {/* {departments} */}
      </div>
    );

    return (
      <div {...sliderStyles.slider.container}>
        <div style={{ transition: "all 2s ease", opacity: this.state.opacity2 }}>
          {content}
        </div>
      </div>
    );
  }
}

export default connect((state, ownProps) => ({
  // user: state.user
}), dispatch => ({
  // userAuth: (userAuth) => dispatch({ type: "USER_AUTH" }),
}))(Slider);