import { style, css, nthChild } from 'glamor'

const styles = {
  desktop: {
    widthStyle: style({ width: '1024px' }),
    gridStyle: style({
      display: 'grid',
      gridTemplateRows: 'auto',
      gridTemplateColumns: '512px 512px'
    }),

    form: {
      label: style({
        display: 'flex',
        flexDirection: 'right',
        // border: "1px solid #ff0000",
        fontFamily: "'Futura Light'",
        fontSize: '1.1rem',
        margin: '0px 0px 10px 0px',
        padding: '0px'
        // ":last-child": {
        //   margin: "0"
        // }
      }),
      clearTextLabel: style({
        color: '#adadad',
        fontSize: '.9rem',
        lineHeight: '1.5',
        width: '130px',
        display: 'block'
      }),
      clearText: style({
        lineHeight: '1.5',
        fontSize: '1rem'
      }),
      inputSexy: style({
        marginTop: '0px',
        display: 'block',
        border: '0',
        outline: '0',
        borderBottom: '1px solid #4B4B4B',
        fontFamily: "'Futura Light'",
        fontSize: '1rem',
        width: '200px'
        // marginLeft: "20px"
      })
    },

    footer: style({
      width: '100%',
      height: '60px',
      borderTop: '1px solid #222',
      color: '#222',
      fontSize: '10px',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'white'
    })
  },
  tablet: {},
  mobile: {},
  common: {
    linkHover: style({
      transition: 'all .5s ease',
      cursor: 'pointer',
      color: '#3eb0ff',
      ':hover': {
        color: '#ffac3e'
      }
    }),
    footerLinkHover: style({
      transition: 'all .5s ease',
      cursor: 'pointer',
      color: '#d5ff3e',
      ':hover': {
        color: '#ffac3e'
      }
    })
  }
}

export default styles
