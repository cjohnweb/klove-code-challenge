import React, { Component } from 'react';
import { connect } from 'react-redux';

import { css, style, nthChild } from 'glamor';

class TopBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      direction: true
    };

    this.windowResize = this.windowResize.bind(this);

  }


  componentWillMount() {
    window.addEventListener("resize", this.windowResize);
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.windowResize);
  }


  windowResize() {
    console.log("Window Resize");
    this.forceUpdate()
  }

  render() {

    // Responsive Styles
    let widthStyle;
    let logoWidthStyle;
    let gridStyle = css({ display: 'grid', gridTemplateRows: 'auto' });


    // Responsive Action
    widthStyle = style({ width: '1024px' });
    gridStyle = css(gridStyle, {
      gridTemplateColumns: '512px 512px'
    });
    logoWidthStyle = css({ width: '350px' })


    // Responsive Break Points
    const breakpoints = {
      desktop: 1040,
      tablet: 840,
      mobile: 540
    };

    if (window.innerWidth >= breakpoints.desktop) {

      widthStyle = style({ width: '1024px' });
      gridStyle = css(gridStyle, {
        gridTemplateColumns: '512px 512px'
      });

    } else if (window.innerWidth >= breakpoints.tablet) {

      widthStyle = style({ width: '840px' });
      gridStyle = css(gridStyle, {
        gridTemplateColumns: '420px 420px',
      });


    } else if (window.innerWidth >= breakpoints.mobile) {

      widthStyle = style({ width: '540px' });
      gridStyle = css(gridStyle, {
        gridTemplateColumns: '270px 270px'
      });

    } else if (window.innerWidth <= breakpoints.mobile) {

      widthStyle = style({ width: '540px' });
      gridStyle = css(gridStyle, {
        gridTemplateColumns: '270px 270px'
      });

    }



    // Non-Responsive Styles

    let fullWidthStyle = style({ width: '100%' })

    let fontFamily = style({ fontFamily: 'Open Sans,Helvetica,Arial' });

    let gridStyleAuto = style({
      display: 'grid',
      gridTemplateColumns: 'auto auto auto',
      gridTemplateRows: 'auto'
    });


    return (<div style={{ width: '100%', background: '#39394d', height: '40px' }}>
      <div {...widthStyle} style={{ margin: '0px auto' }}>
        <div style={{ color: '#efefef', fontSize: '.8rem', textAlign: 'right', paddingTop: '10px' }}>

          <ul style={{ listStyleType: 'none' }}>

            <li style={{ display: 'inline', marginRight: '20px' }}>(530) 346-3801</li>

            <li style={{ display: 'inline', marginRight: '20px' }}>Request Demo</li>

            <li style={{ display: 'inline', marginRight: '20px' }}>Support</li>

          </ul>

        </div>
      </div>
    </div>);

  }
}

export default TopBar;
