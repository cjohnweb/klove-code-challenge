import React, { Component } from 'react';
import { connect } from 'react-redux';
import { css, style, nthChild } from 'glamor';
// import 'font-awesome/css/font-awesome.min.css';
import LandingPage from './landingPage';

class NetworkServices extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  componentDidMount() {
    // This is where we check for user authentication every so many seconds
  }

  render() {
    return (<LandingPage />)
  }

}

export default connect((state, ownProps) => ({
  user: state.user
}), dispatch => ({
  validateAccount: () => dispatch({ type: "VALIDATE_ACCOUNT" }),
  getUser: () => dispatch({ type: "GET_USER" })
}))(NetworkServices);
