import { style, css, nthChild } from 'glamor';

const styles = {

  slider: {
    container: style({
      position: "fixed",
      padding: "0",
      margin: "0",
      top: "0",
      left: "0",
      width: "100%",
      height: "100%",
      fontFamily: "'Futura Light', Arial",
      color: "#000000",
      backgroundSize: "cover",
      WebkitBackgroundSize: "cover",
      MozBackgroundSize: "cover",
      OBackgroundSize: "cover"
    }),
    main: style({
      width: "100%",
      height: "Calc(100% - 64px)",
      overflow: "hidden"
    }),
    slideContainer: style({
      height: "100%",
      width: "100%",
      overflow: "hidden",
      position: "fixed",
      top: "0",
      left: "0",
      width: "100%",
      height: "100%",
      zIndex: "1",
    }),
    slideStatic: style({
      height: "100%",
      width: "100%",
      margin: "0px",
      padding: "0px",
    }),
    slideRight: style({
      position: "relative",
      display: "grid",
      height: "100%",
      width: "200%",
      gridTemplateColumns: "50% 50%",
      gridTemplateRows: "100%",
      left: "-100%"
    }),
    slideLeft: style({
      position: "relative",
      display: "grid",
      height: "100%",
      width: "200%",
      gridTemplateColumns: "50% 50%",
      gridTemplateRows: "100%",
      left: "0"
    }),
    slideUp: style({
      position: "relative",
      display: "grid",
      height: "200%",
      width: "100%",
      gridTemplateRows: "50% 50%",
      gridTemplateColumns: "100%",
      top: "-100%"
    }),
    slideDown: style({
      position: "relative",
      display: "grid",
      height: "200%",
      width: "100%",
      gridTemplateRows: "50% 50%",
      gridTemplateColumns: "100%",
      top: "0%"
    })
  }
}