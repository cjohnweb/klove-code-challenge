import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import Main from './components/main';
// disable service worker
// import registerServiceWorker from './registerServiceWorker';
import 'font-awesome/css/font-awesome.min.css'



window.addEventListener("DOMContentLoaded", () => {

  // const REACT_VERSION = React.version;
  // ReactDOM.render(
  //   <div>React version: {REACT_VERSION}</div>,
  //   document.getElementById('root')
  // );

  let root = document.getElementById("root");
  ReactDOM.render(<Provider store={store}><Main /></Provider>, root);
});

// disable service worker
// registerServiceWorker();
