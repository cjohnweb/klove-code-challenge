const Gitlab = (state = ['Engineering','Software','Human Resources'] , action) => {
    let newState;
    switch (action.type) {
        case "RECEIVE_DEPARTMENTS":
            newState = Object.assign([], action.department);
            return newState;
        default:
            return state;
    }
};

export default Gitlab;