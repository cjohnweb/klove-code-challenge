const Github = (
  state = [
    {
      name: 'John Minton',
      position: 'Frontend Developer',
      company: 'Educational Media Foundation - K-LOVE & Air1',
      phone: '(530) 454-5646',
      email: 'cjohnweb@gmail.com',
      department: 'software'
    },

    {
      name: 'James Mason',
      position: 'Engineering Applications Supervisor',
      company: 'Educational Media Foundation - K-LOVE & Air1',
      phone: '(916) 251-1835',
      email: 'jmason@kloveair1.com',
      department: 'engineering'
    }
  ],
  action
) => {
  let newState
  switch (action.type) {
    case 'RECEIVE_EMPLOYEES':
      newState = Object.assign([], action.employee)
      return newState
    default:
      return state
  }
}

export default Github
