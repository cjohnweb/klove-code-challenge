import { combineReducers } from 'redux';
import Departments from './departments';
import Employees from './employees';

const RootReducer = combineReducers({
    departments: Departments,
    employees: Employees
});

export default RootReducer;