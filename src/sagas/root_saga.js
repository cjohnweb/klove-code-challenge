import { takeEvery, put, call, select } from 'redux-saga/effects'
// import { sendEmail } from '../api/api'

// Get the gitlab authentication token
function * watchCreateEmployee () {
  yield takeEvery('CREATE_EMPLOYEE', function * (action) {
    let result = action.employee
    console.log('CREATE_EMPLOYEE: ', result)
    yield put({ type: 'RECEIVE_EMPLOYEE', result })
  })
}
function * watchUpdateEmployee () {
  yield takeEvery('UPDATE_EMPLOYEE', function * (action) {
    let result = action.employee
    
    console.log('UPDATE_EMPLOYEE: ', result)
    yield put({ type: 'RECEIVE_EMPLOYEE', result })
  })
}
function * watchDeleteEmployee () {
  yield takeEvery('DELETE_EMPLOYEE', function * (action) {
    let result = action.employee
    
    console.log('DELETE_EMPLOYEE: ', result)
    yield put({ type: 'RECEIVE_DELETE_EMPLOYEE', result })
  })
}

// // Get the github authentication token
// function * watchAuthGithub () {
//   yield takeEvery('AUTH_GITHUB', function * () {
//     let auth = yield call(getGithubAuth)
//     // console.log("SAGA GET_USER: ", user);
//     if (!auth || !auth.error) {
//       let auth = { error: auth.error.detail || JSON.stringify(auth.error) }
//     }
//     yield put({ type: 'RECEIVE_GITHUB_AUTH', auth })
//   })
// }

// // Get the github authentication token
// function * watchUpdateGitlab () {
//   yield takeEvery('UPDATE_GITLAB', function * () {
//     let data = yield call(getGitlabUpdate)
//     if (data && !data.error) {
//       yield put({ type: 'RECEIVE_GITLAB_UPDATE', data })
//     } else {
//       alert('There was an error fetching Gitlab Update')
//     }
//   })
// }

// // Get the github authentication token
// function * watchUpdateGithub () {
//   yield takeEvery('UPDATE_GITHUB', function * () {
//     let auth = yield call(getGithubUpdate)
//     // console.log("SAGA GET_USER: ", user);
//     if (!auth || !auth.error) {
//       let auth = { error: auth.error.detail || JSON.stringify(auth.error) }
//     }
//     yield put({ type: 'RECEIVE_GITHUB_UPDATE', auth })
//   })
// }

function * RootSaga () {
  yield [
    watchUpdateEmployee()
  ]
}

export default RootSaga
