export function isURLValid (url) {
  if (url.slice(0, 7) != 'http://' && url.slice(0, 8) != 'https://') {
    return false
  } else {
    return true
  }
}

export function formatURL (site) {
  let url, link, isURL, payload, isB64

  if (site.length) {
    if (site.slice(0, 7) == 'http://') {
      isURL = true
      isB64 = false
      url = site
      link = site.slice(7)
    } else if (site.slice(0, 8) == 'https://') {
      isURL = true
      isB64 = false
      url = site
      link = site.slice(8)
    } else if (
      site.slice(0, 7) != 'http://' &&
      site.slice(0, 8) != 'https://'
    ) {
      isURL = false
      payload = site
      url = ''
      link = ''
      try {
        window.atob(payload)
        isB64 = true
      } catch (e) {
        isB64 = false
        // if you want to be specific and only catch the error which means
        // the base 64 was invalid, then check for 'e.code === 5'.
        // (because 'DOMException.INVALID_CHARACTER_ERR === 5')
        if (e.code === 5) {
          // set error message? idk...
        }
      }
    }
  } else {
    isURL = ''
    isB64 = ''
    url = ''
    link = ''
    payload = ''
  }
  return { isURL, isB64, url, link, payload }
}
