import { createStore, applyMiddleware, compose } from 'redux';
import RootReducer from './reducers/root_reducer.js';
import RootSaga from './sagas/root_saga.js';
import createSagaMiddleware from 'redux-saga';

const sagaMiddleware = createSagaMiddleware();

export default createStore(
    RootReducer,
    /* The following line is to use Redux DevTools extension in Google chrome*/
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    compose(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(RootSaga);
